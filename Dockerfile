FROM golang:1.19-alpine AS build

MAINTAINER BenV

WORKDIR /benv

COPY src/go.* ./

RUN apk --no-cache add git ca-certificates gcc musl-dev && \
	go mod download

COPY src/* ./
RUN go build -o /benvbot

FROM alpine:latest
COPY --from=build /benvbot /benvbot

ENTRYPOINT [ "/benvbot" ]
