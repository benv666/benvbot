.PHONY: all

all: docker

docker:
	 docker build --rm --tag benvbot:latest .

modtidy:
	 docker run --rm -v $(PWD)/src:/go/src golang:1.19-alpine sh -c "cd /go/src ; go mod tidy -v"

clean:
	 docker rm $$(docker ps -a | grep Exited | cut -f1 -d' ')
	 docker rmi $$(docker images | grep '^<none>' | awk '{print $$3;}')

run:
	# docker run --rm -it benvbot:latest benvbot
	docker-compose up
