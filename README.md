# benvbot

Discord bot

## Name
BenVBot

## Description
Pulls the latest beaconchain blocks and parses graffiti data, drops update in channel whenever pixels are painted.

## Usage
To run yourself:
Create .env file with:
```
TOKEN=<discord bot token>
BEACON_URL=http://eth2:5052/
CHANNELID=<numeric channel id, right click and copy ID in discord>
GUILDID=<numeric server id, right click and copy ID in discord>
LISTEN=0
BROADCAST=1
```
docker-compose up

To use our bot:
Invite to your server with https://discord.com/api/oauth2/authorize?client_id=923208828881870848&permissions=277025442816&scope=bot%20applications.commands

